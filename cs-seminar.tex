\documentclass[article]{aaltoseries}
\usepackage[utf8]{inputenc}
\usepackage{hyperref}
\usepackage{blindtext}
\usepackage{todonotes}
\usepackage[ampersand]{easylist}
\usepackage[export]{adjustbox}

\begin{document}

%=========================================================

\title{CoAP Congestion control and DoS attacks}

\author{Luong Khiem Tran% Your first and last name: do _not_ add your student number
\\\textnormal{\texttt{luong.tran@aalto.fi}}} % Your Aalto e-mail address

\affiliation{\textbf{Tutor}: Mohit Sethi} % First and last name of your tutor

\maketitle

\section{Introduction}

% Make this simple enough
The expression Internet of Things (IoT) was first introduced in 1999 \cite{ashton2009thatiot}.
At that time, Things were Radio frequency identification (RFID) tags which are simply used for automatically identifying and tracking objects attached with the tags.
In recent years, there have been increasing numbers of developed IoT solutions.
For examples, users can remotely control and monitor fridges, lights and doors with smart home applications or smart energy and building automation system have been deployed in different buildings for effectively consuming power or providing adaptive ventilation.
According to \cite{betzler2016coap}, by the end of 2020, at least 25 billion objects or devices are expected to be connected to the Internet.
The communication for those devices can be developed based on different protocols depending on different requirements.
The Constrained Application Protocol (CoAP) has been designed and standardized by the Internet Engineering Task Force (IETF) to provide a simplified Hypertext Transfer Protocol (HTTP) interaction between IoT devices.

CoAP is located at the application layer and developed based on the Representational State Transfer (REST) architecture.
% Constrained Application Protocol (CoAP) is standardize by IETF to allow manipulate resources on constrained devices.
Unlike HTTP based on Transmission Control Protocol (TCP), CoAP messages are transfered based on the unreliable datagram-oriented transport layer such as the User Datagram Protocol (UDP).
However, UDP has been used in different Denial-of-Service (DoS) attack mechanisms because UDP does not require return routability check.
For instance, according to \cite{cisco2014securitynotice}, Network Time Protocol (NTP) Servers running on UDP have been used for Distributed Denial-of-Service (DDoS) attacks.
The magnitudes of responses from those NTP servers were 19 times larger than the original request.
Besides, because CoAP relies on the UDP transport protocol, it also faces another challenge in congestion control to ensure the reliability of packet sending.
There are some mechanisms for congestion control, such as basic congestion control of CoAP \cite{rfc7252} (Default CoAP), CoAP simple congestion control/advanced (CoCoa) \cite{ietf-core-cocoa-03} and recently FASOR \cite{jarvinenfasor} which are analyzed in this article.

This article analyzes two challenges of using CoAP for IoT solutions including vulnerabilities to DoS attacks and congestion control solutions for CoAP packets.

The next section, Section 2, introduces fundamental characteristics of CoAP, DoS and congestion control problem over networks. Section 3 presents and analyzes different CoAP vulnerabilities to DDoS attacks. Section 4 presents and analyzes various congestion control challenges and solutions over UDP that can be applied to CoAP deployment. Finally, Section 5 concludes the works of this article.

\section{Background}
\subsection{Constrained Application Protocol CoAP}
% From Wiki and IP lecture
\begin{figure}
	\centering
  \includegraphics[width=0.7\linewidth]{figures/CoAP-stack.png}
  \captionsetup{justification=centering}
  \caption{CoAP Protocol Stack}
  \label{fig:coap-stack}
\end{figure}

\begin{figure}
	\centering
  \includegraphics[width=0.7\linewidth]{figures/CoAP-features.png}
  \captionsetup{justification=centering}
  \caption{CoAP features \cite{rfc7252}, \cite{rfc4944}}
  \label{fig:coap-features}
\end{figure}

This section provides overall main features of CoAP which are utilized for the analysis in next sections.
Figure \ref{fig:coap-stack} illustrates the protocol stack where CoAP is based on.
Figure \ref{fig:coap-features} describes details of four features of CoAP including supporting Web protocol, UDP binding, supporting Machine-to-machine requirements and applied on constrained environment.

CoAP is a protocol at the application layer of IoT applications.
CoAP is designed to run with resource-constrained devices, on constrained networks, and be able to integrate with the existing Web through HTTP translation \cite{rfc7252}.
The resource-constrained devices often have 8-bit microcontrollers with small storage capacity of ROM and RAM.
The current standard for constrained networks is Internet Protocol version 6 (IPv6) over Low-Power Wireless Personal Area Networks (6LoWPAN).
The 6LoWPAN is the design specification for network environments with high packet error rates, using 127 bytes data frame and low throughput \cite{rfc4944}.
The REST model of HTTP is applied for one-to-one communication between CoAP devices with four supported methods GET, POST, PUT and DELETE.

However, unlike HTTP, CoAP supports IoT specialized requirements focusing on machine-to-machine (M2M) applications.
The requirements include built-in discovery of services and resources discovery, multicast, very low overhead and simplicity for constrained environments.
M2M applications that operate autonomously with less manual human efforts depend upon multicast requests to explore the services and resources directory.
CoAP endpoints that offer multicast service discovery need to join the group of all-CoAP-node multicast addresses including 224.0.1.187 for Internet Protocol version 4 (IPv4) and FF0x::FD for IPv6.
The multicast feature of CoAP can be only enabled by utilizing UDP at the transport layer which supports the use of multicast Internet Protocol (IP) destination addresses.

CoAP transmissions are secured with Datagram Transport Layer Security (DTLS) protocol at the session layer similar to Transport Layer Security (TLS) for HTTPS.
There are four security modes of for a CoAP device:
\begin{easylist}[itemize]
 & NoSec: DTLS is disabled. This mode can be used for unimportant transmissions or used with Internet Protocol Security (IPsec). The NoSec mode does not require encryption processes, thus less energy consumption for constrained devices. However, this mode should be utilized deliberately or there are many security risks exposed, especially the risk of DoS attacks which is discussed in section \ref{sect:coap-ddos}.
 & PreSharedKey: DTLS is enabled with a list of pre-shared keys. Each key is attached with a list of nodes that the device can use the key to communicate.
 & RawPublicKey: DTLS is enabled with one asymmetric key pair without a certificate for the device and a list of public keys of nodes that the device can communicate with.
 & Certificate: Similar to RawPublicKey but each device also has an X.509 certificate for the key pair and an additional list of root trust anchors for validating certificates of public keys of interacted nodes.
\end{easylist}


UDP which is not designed for reliable transmissions \cite{rfc768}.
Therefore, CoAP utilizes different message types to control reliability levels at the message layer in the figure \ref{fig:coap-stack}.
The confirmable message type is used for reliable transmissions that require retransmitting the messages multiple times until receiving the corresponding Acknowledgement (ACK) packet.
The retransmissions introduce again the traffic congestion that has been already challenged the TCP protocol from the very beginning.
With the large number of IoT devices transferring vast amounts of packets over networks, the congestion control has been already considered from the CoAP standard.
The congestion control mechanisms are discussed more details in the section \ref{sub:congestion-control}.


% This sentence may duplicate with the aim in the introduction.
% This article analyzes the correlation between CoAP and UDP and research efforts against the impacts of distribution denial of service attacks and network congestion control problems.

% Open points about Security / Security considerations from the the standard?
\subsection{Distributed Denial of Service Attacks}
DoS is an attack method that attempts to prevent legitimate users from using a service \cite{stallings2016network}.
Distributed DoS or DDoS attack is the large number of DoS attacks launched from multiple compromised hosts that are remotely controlled by the attacker.
There are multiple targets of a system of for a DoS attack, such as physical devices of the system, computational resources of devices or the communication of the local network of the system \cite{rfc4732}.

Regarding to the selected attacked target, different approaches can be applied for launching a DoS attack.
One approach is interrupting the functionalities of internal system nodes by physical device destruction, forcing devices running out of energy or manipulating the original functions of devices.
Another approach is flooding a vast amount of packets to overload either processing capacity of the target system or the traffic of the local network utilized by the target system.
According to \cite{stallings2016network}, one flooding type is the direct DDoS attack with the packets are all sent from compromised nodes to the victim.
One other flooding type is the reflector DDoS attack which adds another layer of machines called reflectors.
The compromised nodes send requests with spoofed source address, that is the victim address, to the reflectors.
The reflectors respond each request to the victim with some responses or with one response having larger size than the request.
The larger size of responses comparing to the request size, the more critical of the attack.
The ratio between the size of responses and the request size is the amplification factor of the DDoS attack.
These discussed attack approaches are realized on CoAP systems in the section \ref{sect:coap-ddos}.

In this article, the DoS attack over CoAP systems is analyzed in two aspects.
The first aspect is the DoS attack direct to the CoAP distributed system itself.
The second aspect is the compromised CoAP devices are possibly utilized for DDoS attacks to other systems.
\subsection{Network Congestion Control}
% A writing flow => coherent => a full pictures.
% Mình có thể đưa vào những điểm nhỏ trước luôn
% Chưa cần đánh giá vội
The communications between hosts are based on packet transmissions from sources, through different interconnected nodes before coming to the destination \cite{Stallings:2013:DCC:2566715}.
% interconnected nodes
The middle nodes process forwarding data to following nodes with the difference between input and output transmission rates.
Queuing and dropping packets are mechanisms for controlling data transmission that is over capacity of a middle node.
Congestion happens when there are packet loss or timeout due to queuing delay that lead to retransmit packets from sources.

As specified in the UDP usage guidelines \cite{rfc8085}, congestion control is the most essential part of maintaining the stability of the Internet. Applications or protocols that operate over UDP transport layer must implement mechanisms to restrain congestion collapse and to ensure some extents of fairness with other coexisting traffic.

The UDP usage guidelines provide specific directions for different application traffic types including applications performing bulk transfers, low data-volume applications or applications supporting bidirectional communications.

Applications that transfer more than a small number of UDP datagrams per RTT are considered bulk transfer applications.
These applications should employ TCP-Friendly Rate Control (TRFC), window-based, TCP like congestion control, or at least comply with the congestion principles \cite{rfc2914}.

Low data-volume applications can be specified by at any time transferring only a small number of UDP datagrams with a destination.
Those applications should still control the transfer rate on average about one UDP datagram per RTT. For packet loss retransmission, three considered components include RTT estimation for each destination, packet loss detection and exponentially back-off retransmission timeout. The IETF standard \cite{rfc6298} based on Karn's algorithm \cite{karn1987improving} is a comprehensive recommendation for the RTT estimation mechanism. Applications that cannot maintain a reliable RTT estimate can apply different following approaches:
\begin{easylist}[itemize]
  & Utilizing predefined retransmission timeout which is exponentially backed-off on having packet lost. Default RTO of CoAP is 2 second.
  & For the exchanges without return traffic, applications should limit sending rate to one UDP datagram per 3 seconds or less.
\end{easylist}

Applications that bidirectionally exchange packets should implement congestion control from both sides.
Client-server model with request-response-style is a type for these applications.
To control congestion, both client and server should independently detect and respond to congestion of the transmissions, utilize acknowledged responses across the entire round-trip path for limiting retransmitted or new requests.

The above congestion control guidelines have been applied in designing various congestion control solutions to support CoAP applications. 
The section \ref{} discusses these solutions in more details.
\section{CoAP Vulnerabilities to DDoS Attacks}
\label{sect:coap-ddos}

\subsection{IP Address spoofing on UDP and Risk of Amplification of CoAP packets}
The CoAP standard \cite{rfc7252} has pointed out that 

This research \cite{federico2018iotdatabackbone} has indicated that the request/response protocol like CoAP allows attackers manipulate the reflector to flood UDP packets to the victim with less obstruction from firewalls.
Attackers in this case can spoof the IP address of the victim before sending requests to the reflector.
The reflector running CoAP applications trusts the IP address of the victim then responding all bogus requests to the victim.
Moreover, statistics from this report \cite{StateofI1:online} demonstrates that there are about 16 percent of the IPv4 and IPv6 blocks and over 20 percent of the IPv4 and IPv6 autonomous systems are still susceptible to spoofing.

In a different circumstance, the research \cite{federico2018iotdatabackbone} has identified the amplification risk of request/response protocols running over UDP and estimated the bandwidth amplification factor (BAF) for CoAP usage.
The root cause of this amplification risk is the CoAP responses can be significantly larger than the requests.
In the case that an attacker is able to POST or PUT a very large content to a reflector, then generating multiple spoofed requests to that content.
Although the block-wise transfer feature of CoAP can be applied to divide a large response into smaller ones, the attacker can request configuration to the maximum block size.
The amplification factor increases to 32x times compare to the request size.
Recorded amplification attacks utilized Domain Name System (DNS) and Simple Service Discovery Protocol (SSDP) with the BAF ranges between 28x and 98x times of 1Mbps bandwidth of sending requests.

\subsection{Resource Discovery}

\subsection{Publisher and Subscriber for CoAP}

\subsection{Discussion}


\section{Congestion Control Solutions for CoAP}
\label{sub:congestion-control}
\subsection{Default CoAP congestion control}


\subsection{CoAP simple congestion control}


\subsection{FASOR}


\subsection{Discussion}

\section{Conclusion}

\bibliographystyle{plain}
\bibliography{cs-seminar}

\end{document}
